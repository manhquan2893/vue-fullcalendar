import {REGISTER,LOGIN,
    CHECK_AUTH,LOGOUT} from '@/store/action.type'
import {SET_USER} from '@/store/mutation.type'
import JwtService from '@/common/Jwt.service'
import {ApiService} from '@/common/Api.service'
import router from '@/router'
export default {
    state: {
        value: 'my value',
        isAuthenticated:!!JwtService.getToken(),
        user:''
    },
    getters: {
        isAuthenticated: state=>{
            return state.isAuthenticated;
        },
        user:state => {
            return state.user;
        }
    },
    mutations: {
        [SET_USER](state,user){
            state.isAuthenticated=true
            state.user=user
        }
    },
    actions: {
        [REGISTER](context,user){
            return new Promise((resolve,reject)=>{
                ApiService.register(user)
                .then((res)=>{
                    JwtService.setToken(res.data.token)
                    resolve()
                })
                .catch(({response})=>{
                    reject(response.data.errors)
                })
            })
        },
        [LOGIN](context,user){
            return new Promise((resolve,reject)=>{
                ApiService.login(user)
                .then((res)=>{
                    JwtService.setToken(res.data.token)
                    resolve()
                })
                .catch(({response})=>{
                    console.log(response)
                    reject(response.data.errors)
                })
            })
        },
        [CHECK_AUTH](context){
            return new Promise((resolve,reject)=>{
                if(JwtService.getToken()){
                    ApiService.setHeader()
                    ApiService.getUser()
                    .then((res)=>{
                        context.commit(SET_USER,res.data)
                        resolve()
                    })
                }
                else{
                    reject()
                }
            })
        },
        [LOGOUT](){
            JwtService.removeToken();
            router.push({name:'home'});
        }
    }
};