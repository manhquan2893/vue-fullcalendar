import {GET_ALL_USERS,GET_ALL_EVENTS,
     CREATE_EVENT,EDIT_EVENT,
     GET_USER_BY_ID,
     TOGGLE_FINISHED_EVENT,
     DELETE_EVENT} from './action.type'
import {SET_LIST_EVENT, ADD_ONE_EVENT,
    EVENT_EDIT,
    SET_LIST_USER,
    EVENT_TOGGLE_FINISHED} from './mutation.type'

import { ApiService } from '@/common/Api.service'
export default {
    state: {
        listevent:[],
        listuser:[]
    },
    getters:{
        listevent: state => state.listevent,
        getEventById: (state) => (id) =>{
            return state.listevent.find((el)=>{ return el.id == id})
        }
    },
    mutations: {
        [SET_LIST_EVENT](state,_listevent){
            state.listevent= _listevent
        },
        [ADD_ONE_EVENT](state,event){
            state.listevent.push(event)
        },
        [EVENT_EDIT](state,event){
            let listevent= state.listevent
            for (let i = 0; i < listevent.length; i++) {
                if(listevent[i].id==event.id){
                    listevent[i] = event;
                }
            }
        },
        [SET_LIST_USER](state,_listuser){
            state.listuser= _listuser
        },
        [EVENT_TOGGLE_FINISHED](state,event_id){
            let listevent= state.listevent
            for (let i = 0; i < listevent.length; i++) {
                if(listevent[i].id==event_id){
                    if(listevent[i].isfinished==0){
                        listevent[i].isfinished=1
                    }
                    else{
                        listevent[i].isfinished=0
                    }
                }
            }
        }
        // use id to get event on state (call directly, not via action)
        
    },
    actions: {
        [GET_ALL_USERS](context){
            return new Promise((resolve,reject)=>{
                ApiService.getAllUsers()
                .then((res)=>{
                    context.commit(SET_LIST_USER,res.data)
                    resolve(res.data)
                })
                .catch((err)=>{
                    reject(err.response)
                })
            })
        },
        [CREATE_EVENT](context,data){
            return new Promise((resolve)=>{
                ApiService.createEvent(data.event)
                .then(({data})=>{
                    context.commit(ADD_ONE_EVENT,data)
                    resolve(data)
                })
            })
        },
        [EDIT_EVENT](context,data){
            return new Promise((resolve)=>{
                ApiService.editEvent(data.event)
                .then(({data})=>{
                    context.commit(EVENT_EDIT,data)
                    resolve(data)
                })
            })
        },
        [GET_ALL_EVENTS](context){
            return new Promise((resolve)=>{
                ApiService.getAllEvents()
                .then((res)=>{
                    context.commit(SET_LIST_EVENT,res.data)
                    resolve()
                })
            })
        },
        [GET_USER_BY_ID](context,user_id){
            return new Promise((resolve)=>{
                ApiService.getUserById(user_id)
                .then(({data})=>{
                    resolve(data)
                })
            })
        },
        [TOGGLE_FINISHED_EVENT](context,event_id){
            return new Promise((resolve)=>{
                ApiService.toggleFinishedEvent(event_id)
                .then(({data})=>{
                    context.commit(EVENT_TOGGLE_FINISHED,data.id)
                    resolve(data)
                })
            })
        },
        [DELETE_EVENT](context,event_id){
            return new Promise((resolve)=>{
                ApiService.deleteEvent(event_id)
                .then(()=>{
                    resolve()
                })
            })
        }
    }
};