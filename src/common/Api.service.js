import axios from 'axios'
axios.defaults.baseURL = 'http://localhost/apicalendar/public/api/'
export const ApiService={
    setHeader(){
        axios.defaults.headers.common['Authorization'] = `Bearer ${localStorage.getItem('token')}`;
    },
    register(credentials){
        return axios.post('register',credentials);
    },
    login(credentials){
        return axios.post('login',credentials);
    },
    getAllUsers(){
        return axios.get('users');
    },
    //use user_id to get username
    getUserById(user_id){
        return axios.get(`user/${user_id}`);
    },
    //use header authentication token to get User
    getUser(){
        return axios.get('user');
    },
    createEvent(event){
        return axios.post('events',event);
    },
    getAllEvents(){
        return axios.get('events');
    },
    editEvent(event){
        return axios.put('event',event)
    },
    toggleFinishedEvent(event_id){
        return axios.put('toggleFinishedEvent',{event_id})
    },
    deleteEvent(event_id){
        return axios.delete(`event/${event_id}`)
    }
}
