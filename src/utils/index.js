import moment from 'moment'
export const utils={
    convertToCalendarFormat(event){
        let _event={};
        _event.title=event.title
        _event.description=event.description
        _event.start= event.startdate.split('-').reverse().join('-') +'T'+event.starttime
        _event.end=event.enddate.split('-').reverse().join('-') +'T'+event.endtime
        _event.id=event.id
        _event.isfinished=event.isfinished
        _event.listviewer=event.listviewer
        _event.user_id=event.user_id
        return _event;
    },
    convertFromCalendarFormat(event){
        let _event={}
        _event.title=event.title
        _event.description=event.extendedProps.description
        _event.startdate=moment(event.start).format('DD-MM-YYYY')
        _event.enddate=moment(event.end).format('DD-MM-YYYY')
        _event.starttime=moment(event.start).format('HH:mm')
        _event.endtime=moment(event.end).format('HH:mm')
        _event.id=event.id
        _event.viewers=event.extendedProps.listviewer
        _event.user_id=event.extendedProps.user_id
        _event.isfinished=event.extendedProps.isfinished
        return _event
    },
    setColorForEvent(event,user_id){
        if(event.isfinished==1){
            event.color="green"
        }
        if(event.user_id != user_id){
            event.color="purple"
        }
    }
}
export default utils