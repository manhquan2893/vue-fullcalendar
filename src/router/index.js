import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
    routes:[
        {
            name:'calendar',
            path:'/',
            component:()=>import("@/views/Calendar")
        },
        {
            name:'home',
            path:'/home',
            component:()=>import("@/views/Home"),
            redirect:{name:'login'},
            children:[
                {
                    name:'login',
                    path:'/login',
                    component:()=>import("@/components/Login")
                },
                {
                    name:'register',
                    path:'/register',
                    component:()=>import("@/components/Register")
                }
            ]
        },
        
    ]
})