 const filters = {
    capitalize: function(value) {
        if (!value) return ''
        value = value.toString()
        return value.charAt(0).toUpperCase() + value.slice(1)
    },
    visibility:{
        all: function(events){
            return events
        },
        myevent: function(events,user_id){
            return events.filter((el)=>{
                return el.user_id == user_id
            })
        },
        otherevent:function(events,user_id){
            return events.filter((el)=>{
                return el.user_id != user_id
            })
        }
    }   
}
export default filters